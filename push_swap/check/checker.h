/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ylila <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/21 23:10:03 by ylila             #+#    #+#             */
/*   Updated: 2020/01/21 23:10:14 by ylila            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHECKER_H
# define CHECKER_H

# include "get_next_line.h"
# include "validate_input.h"
# include "../vizualization/vizual.h"

_Bool	check_instruction(const char *instr, t_stack **head_a,
							t_stack **head_b);
_Bool	is_instruction(const char *instr);

#endif
