/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ylila <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/21 23:10:22 by ylila             #+#    #+#             */
/*   Updated: 2020/01/22 20:51:40 by ylila            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"
#include "push_swap.h"

_Bool		check_instruction(const char *instr, t_stack **head_a,
								t_stack **head_b)
{
	if (!is_instruction(instr))
		return (0);
	if (!(ft_strcmp(instr, "sa")))
		swap(head_a, 0, 0);
	else if (!(ft_strcmp(instr, "sb")))
		swap(head_b, 0, 0);
	else if (!(ft_strcmp(instr, "ss")))
		swap_swap(head_a, head_b, 0, 0);
	else if (!(ft_strcmp(instr, "pa")))
		push(head_a, head_b, 0);
	else if (!(ft_strcmp(instr, "pb")))
		push(head_b, head_a, 0);
	else if (!(ft_strcmp(instr, "ra")))
		rotate(head_a, 0, 0);
	else if (!(ft_strcmp(instr, "rb")))
		rotate(head_b, 0, 0);
	else if (!(ft_strcmp(instr, "rr")))
		rotate_rotate(head_a, head_b, 0, 0);
	else if (!(ft_strcmp(instr, "rra")))
		reverse_rotate(head_a, 0, 0);
	else if (!(ft_strcmp(instr, "rrb")))
		reverse_rotate(head_b, 0, 0);
	else if (!(ft_strcmp(instr, "rrr")))
		reverse_rotate_rotate(head_a, head_b, 0, 0);
	return (1);
}

_Bool		is_instruction(const char *instr)
{
	return (!ft_strcmp(instr, "sa") || !ft_strcmp(instr, "sb") ||
			!ft_strcmp(instr, "ss") || !ft_strcmp(instr, "pa") ||
			!ft_strcmp(instr, "pb") || !ft_strcmp(instr, "ra") ||
			!ft_strcmp(instr, "rb") || !ft_strcmp(instr, "rr") ||
			!ft_strcmp(instr, "rra") || !ft_strcmp(instr, "rrb") ||
			!ft_strcmp(instr, "rrr"));
}

static void	read_input(t_stack **head_a, t_stack **head_b, const uint8_t flags)
{
	char	*line;

	line = NULL;
	if (flags & FLG_V)
		print_stacks(*head_a, *head_b);
	while (get_next_line(0, &line) > 0)
	{
		if (!*line)
			break ;
		if (!(check_instruction(line, head_a, head_b)))
		{
			free_stack(head_a);
			free_stack(head_b);
			print_error(0);
		}
		if (flags & FLG_V)
			print_stacks(*head_a, *head_b);
		free(line);
	}
}

static void	read_input_viz(t_stack **head_a, t_stack **head_b)
{
	char	*line;
	t_viz	viz;

	line = NULL;
	make_rank(*head_a);
	init_viz(&viz);
	while (viz.is_running)
	{
		handle_events(&viz);
		draw_rects(&viz, *head_a);
		while (get_next_line(0, &line) > 0)
		{
			if (!*line)
				break;
			if (!(check_instruction(line, head_a, head_b)))
			{
				free_stack(head_a);
				free_stack(head_b);
				print_error(0);
			}
			draw_rects(&viz, *head_a);
			free(line);
		}
	}
	quit_viz(&viz);
}

int			main(int ac, char **av)
{
	t_stack	*head_a;
	t_stack	*head_b;
	uint8_t	flags;

	flags = 0;
	head_a = NULL;
	head_b = NULL;
	prepare_args(ac, av, &head_a, &flags);
	flags & FLG_V ? read_input_viz(&head_a, &head_b) :
				read_input(&head_a, &head_b, flags);
	if (!(is_sorted_asc(head_a)))
	{
		free_stack(&head_a);
		free_stack(&head_b);
		print_error(1);
	}
	free_stack(&head_a);
	if (!(is_empty(head_b)))
	{
		free_stack(&head_b);
		print_error(0);
	}
	ft_printf("OK\n");
	return (0);
}
