//
// Created by da.filiptsev on 23.01.2020.
//

#ifndef VIZUAL_H
# define VIZUAL_H

# include <SDL.h>
# include "stack_intf.h"

typedef struct	s_viz
{
	SDL_Window		*win;
	SDL_Renderer	*rend;
	SDL_Surface		*simple;
	int 			win_w;
	int 			win_h;
	_Bool			is_running;
}				t_viz;

void			init_viz(t_viz *viz);
void			quit_viz(t_viz *viz);
void			draw_rects(t_viz *viz, t_stack *head);
void			handle_events(t_viz *viz);

#endif
