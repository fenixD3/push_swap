//
// Created by da.filiptsev on 23.01.2020.
//

#include "vizual.h"

void	init_viz(t_viz *viz)
{
	SDL_DisplayMode	dm;

	if (SDL_Init(SDL_INIT_EVERYTHING))
		exit (1); /// write SDL_error protection
	if (SDL_GetCurrentDisplayMode(0, &dm))
		exit (1);
	viz->win_w = dm.w - 100;
	viz->win_h = dm.h - 100;
	if (!(viz->win = SDL_CreateWindow("auto_displ", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
			viz->win_w, viz->win_h,SDL_WINDOW_SHOWN)))
		exit (1);
	if (!(viz->rend = SDL_CreateRenderer(viz->win, -1,
			SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC)))
		exit (1);
	viz->is_running = 1;
}

void	draw_rects(t_viz *viz, t_stack *head)
{
	SDL_Rect	rect;
	int			cnt_rect;
	int 		y_coord;

	SDL_SetRenderDrawColor(viz->rend, 0xFF, 0xFF, 0xFF, 0xFF);
	SDL_RenderClear(viz->rend);
	SDL_SetRenderDrawColor(viz->rend, 140, 0, 0xFF, 0);
	cnt_rect = (int)size_stack(head);
	y_coord = 0;
	rect.x = 0;
	rect.h = viz->win_h / cnt_rect;
	while (y_coord <= viz->win_h && head)
	{
		rect.y = y_coord;
		rect.w = ((int)head->indx) * (viz->win_w / cnt_rect);
		SDL_RenderDrawRect(viz->rend, &rect);
		//SDL_RenderFillRect(viz->rend, &rect);
		head = head->next;
		y_coord += viz->win_h / cnt_rect;
	}
	SDL_RenderPresent(viz->rend);
	SDL_Delay(2000);
}

void	quit_viz(t_viz *viz)
{
	SDL_DestroyRenderer(viz->rend);
	viz->rend = NULL;
	SDL_DestroyWindow(viz->win);
	viz->win = NULL;
	SDL_Quit();
}

void	handle_events(t_viz *viz)
{
	SDL_Event	ev;

	SDL_PollEvent(&ev);
	if (ev.type == SDL_QUIT)
		viz->is_running = 0;
}
