/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_three.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ylila <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/22 20:59:13 by ylila             #+#    #+#             */
/*   Updated: 2020/01/22 21:07:20 by ylila            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	three_sort(t_stack **head, const size_t part)
{
	size_t	pos_max;
	size_t	pos_min;

	find_max_ind_part(*head, &pos_max, (*head)->part);
	find_min_ind_part(*head, &pos_min, (*head)->part);
	if (pos_max == 1 && pos_min == 0)
	{
		reverse_rotate(head, part, 1);
		swap(head, part, 1);
	}
	else if (pos_max == 2 && pos_min == 1)
		swap(head, part, 1);
	else if (pos_max == 1 && pos_min == 2)
		reverse_rotate(head, part, 1);
	else if (pos_max == 0 && pos_min == 1)
		rotate(head, part, 1);
	else if (pos_max == 0 && pos_min == 2)
	{
		swap(head, part, 1);
		reverse_rotate(head, part, 1);
	}
}

void	three_sort_under_a(t_stack **head, const size_t part)
{
	size_t	pos_max;
	size_t	pos_min;

	find_max_ind_part(*head, &pos_max, (*head)->part);
	find_min_ind_part(*head, &pos_min, (*head)->part);
	if (pos_max == 1 && pos_min == 0)
		three_und_a_1_0(head, NULL, part);
	else if (pos_max == 2 && pos_min == 1)
		swap(head, part, 1);
	else if (pos_max == 1 && pos_min == 2)
		three_und_a_1_2(head, NULL, part);
	else if (pos_max == 0 && pos_min == 1)
		three_und_a_0_1(head, NULL, part);
	else if (pos_max == 0 && pos_min == 2)
		three_und_a_0_2(head, NULL, part);
}

void	three_sort_under_b(t_stack **hb, t_stack **ha, size_t size_part,
						const size_t part)
{
	push_n_time(ha, hb, size_part, 1);
	three_sort_under_a(ha, part);
}

void	three_reverse_sort(t_stack **hb, t_stack **ha, const size_t part)
{
	size_t	pos_max;
	size_t	pos_min;

	find_max_ind_part(*hb, &pos_max, (*hb)->part);
	find_min_ind_part(*hb, &pos_min, (*hb)->part);
	if (pos_max == 2 && pos_min == 0)
	{
		swap(hb, part, 1);
		reverse_rotate(hb, part, 1);
	}
	else if (pos_max == 1 && pos_min == 0)
	{
		reverse_rotate(hb, part, 1);
		reverse_rotate(hb, part, 1);
	}
	else if (pos_max == 2 && pos_min == 1)
		reverse_rotate(hb, part, 1);
	else if (pos_max == 1 && pos_min == 2)
		swap(hb, part, 1);
	else if (pos_max == 0 && pos_min == 1)
	{
		reverse_rotate(hb, part, 1);
		swap(hb, part, 1);
	}
	push_n_time(ha, hb, size_part(*hb, part), 1);
}
