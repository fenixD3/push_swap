/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   part_intf_2.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ylila <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/21 23:15:05 by ylila             #+#    #+#             */
/*   Updated: 2020/01/22 21:04:12 by ylila            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static size_t	find_pos_elem(t_stack *head, size_t el_ind)
{
	size_t	pos;

	pos = 0;
	while (head && head->indx != el_ind)
	{
		++pos;
		head = head->next;
	}
	return (pos);
}

t_stack			*find_min_ind_part(t_stack *head, size_t *pos,
						const size_t scanned_part)
{
	t_stack *stack;
	t_stack	*min_node;

	if (!head)
		return (NULL);
	stack = head;
	while (stack && stack->part != scanned_part)
		stack = stack->next;
	min_node = stack;
	while (stack)
	{
		if (stack->part == scanned_part && stack->indx < min_node->indx)
			min_node = stack;
		stack = stack->next;
	}
	if (pos)
		*pos = find_pos_elem(head, min_node->indx);
	return (min_node);
}

t_stack			*find_max_ind_part(t_stack *head, size_t *pos,
							const size_t scanned_part)
{
	t_stack *stack;
	t_stack	*max_node;

	if (!head)
		return (NULL);
	stack = head;
	while (stack && stack->part != scanned_part)
		stack = stack->next;
	max_node = stack;
	while (stack)
	{
		if (stack->part == scanned_part && stack->indx > max_node->indx)
			max_node = stack;
		stack = stack->next;
	}
	if (pos)
		*pos = find_pos_elem(head, max_node->indx);
	return (max_node);
}

_Bool			is_sorted_part_desc(t_stack *head, const size_t part)
{
	while (head && head->part == part)
	{
		if (head->next && head->num <= head->next->num)
			return (0);
		head = head->next;
	}
	return (1);
}

_Bool			is_another_part(t_stack *head, const size_t part)
{
	while (head)
	{
		if (head->part != part)
			return (1);
		head = head->next;
	}
	return (0);
}
