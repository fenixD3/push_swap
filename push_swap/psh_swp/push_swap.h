/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ylila <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/22 21:02:00 by ylila             #+#    #+#             */
/*   Updated: 2020/01/22 21:05:43 by ylila            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include "stack_intf.h"

/*
** Genaral sorting intf
*/
size_t	get_pivot_avg_part(t_stack *head, const size_t scaned_part);
void	move_from_to(t_stack **from, t_stack **to, const size_t avg_ind);
void	make_rank(t_stack *head);
void	put_parts(t_stack *head, const size_t scanned_part);
void	if_from_is_b(t_stack **frm, t_stack **to, const size_t max_part);

/*
** Sorting five and three element
*/

void	five_sort_prepare(t_stack **from, t_stack **to, const size_t avg_ind,
							const size_t max_part);
void	five_sort_a(t_stack **from, t_stack **to, const size_t avg_ind,
				const size_t part);
void	five_sort_part_a(t_stack **from, t_stack **to, const size_t avg_ind,
						const size_t part);
void	five_sort_b(t_stack **from, t_stack **to, const size_t avg_ind,
				const size_t part);
void	five_sort_part_b(t_stack **from, t_stack **to, const size_t avg_ind,
							const size_t part);
void	three_sort(t_stack **head, const size_t part);
void	three_sort_under_a(t_stack **head, const size_t part);
void	three_reverse_sort(t_stack **hb, t_stack **ha, const size_t part);
void	three_sort_under_b(t_stack **hb, t_stack **ha, size_t size_part,
						const size_t part);
void	push_n_time(t_stack **dest, t_stack **source, size_t size_part,
						_Bool out);
void	both_round_three(t_stack **ha, t_stack **hb, const size_t part);
void	both_round_three_under_a(t_stack **ha, t_stack **hb, const size_t part);
void	both_round_three_under_b_is_2(t_stack **ha, t_stack **hb,
										const size_t part);
void	both_round_three_under_a_is_2(t_stack **three, t_stack **two,
								const size_t part);
void	sorted_part_to_zero(t_stack **head, const size_t part_to_zero);

/*
** Help for three sorting under
*/

void	three_und_a_1_0(t_stack **ha, t_stack **hb, const size_t part);
void	three_und_a_1_2(t_stack **ha, t_stack **hb, const size_t part);
void	three_und_a_0_1(t_stack **ha, t_stack **hb, const size_t part);
void	three_und_a_0_2(t_stack **ha, t_stack **hb, const size_t part);

/*
** Enhancement stack interface
*/

t_stack	*find_min_ind_part(t_stack *head, size_t *pos,
							const size_t scanned_part);
t_stack	*find_max_ind_part(t_stack *head, size_t *pos,
							const size_t scanned_part);
size_t	size_part(t_stack *head, const size_t counted_part);
t_stack	*find_max_part(t_stack *a, t_stack *b);
_Bool	is_sorted_part_asc(t_stack *head, const size_t part);
_Bool	is_sorted_part_desc(t_stack *head, const size_t part);
_Bool	is_part_in(t_stack *head, const size_t part);
_Bool	is_another_part(t_stack *head, const size_t part);

#endif
