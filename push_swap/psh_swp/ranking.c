/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ranking.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ylila <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/22 21:00:52 by ylila             #+#    #+#             */
/*   Updated: 2020/01/22 21:06:34 by ylila            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int			find_min(t_stack *head)
{
	int		min;

	while (head && head->indx)
		head = head->next;
	if (head)
		min = head->num;
	while (head)
	{
		if (head->indx)
		{
			head = head->next;
			continue ;
		}
		else if (head->num < min)
			min = head->num;
		head = head->next;
	}
	return (min);
}

inline static void	put_rank(t_stack *head, const int min,
						const size_t rank)
{
	while (head->num != min)
		head = head->next;
	head->indx = rank;
}

void				make_rank(t_stack *head)
{
	size_t	size;
	int 	min_num;
	size_t	rank;

	size = size_stack(head);
	rank = 1;
	while (size--)
	{
		min_num = find_min(head);
		put_rank(head, min_num, rank++);
	}
}

static void			write_parts(t_stack *head, t_stack *max, t_stack *min)
{
	while (head && (max || min))
	{
		if (max && head->indx == max->indx)
			head->part = (head->part) ? head->part * 10 + 2 : 2;
		if (min && head->indx == min->indx)
			head->part = (head->part) ? head->part * 10 + 1 : 1;
		head = head->next;
	}
}

void				put_parts(t_stack *head, const size_t scanned_part)
{
	// v 1.0
	/*size_t	avg;

	avg = get_pivot_avg_part(head, scanned_part);
	while (head && head->part == scanned_part)
	{
		if (head->indx > avg)
			head->part = (head->part) ? head->part * 10 + 2 : 2;
		else
			head->part = (head->part) ? head->part * 10 + 1 : 1;
		head = head->next;
	}*/

	size_t	cnt_in_max;
	size_t	cnt_in_min;
	t_stack	*max;
	t_stack	*min;

	cnt_in_min = 5;
	while ((cnt_in_min * 2) < size_part(head, scanned_part))
		cnt_in_min *= 2;
	cnt_in_max = size_part(head, scanned_part) - cnt_in_min;
	while (cnt_in_max > 0 || cnt_in_min > 0)
	{
		max = NULL;
		min = NULL;
		if (cnt_in_max > 0)
		{
			max = find_max_ind_part(head, NULL, scanned_part);
			--cnt_in_max;
		}
		if (cnt_in_min > 0)
		{
			min = find_min_ind_part(head, NULL, scanned_part);
			--cnt_in_min;
		}
		write_parts(head, max, min);
	}
}
