/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   part_intf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ylila <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/22 21:03:04 by ylila             #+#    #+#             */
/*   Updated: 2020/01/22 21:09:31 by ylila            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

size_t	size_part(t_stack *head, const size_t counted_part)
{
	size_t	size;

	size = 0;
	while (head)
	{
		if (head->part == counted_part)
			++size;
		head = head->next;
	}
	return (size);
}

t_stack	*find_max_part(t_stack *a, t_stack *b)
{
	t_stack	*max_part_node;

	max_part_node = a ? a : b;
	while (a || b)
	{
		if (a && a->part > max_part_node->part)
			max_part_node = a;
		else if (b && b->part > max_part_node->part)
			max_part_node = b;
		a = a ? a->next : a;
		b = b ? b->next : b;
	}
	return (max_part_node);
}

void	sorted_part_to_zero(t_stack **head, const size_t part_to_zero)
{
	t_stack	*to_zero;

	to_zero = *head;
	while (to_zero && to_zero->part == part_to_zero)
	{
		to_zero->part = 0;
		to_zero = to_zero->next;
	}
}

_Bool	is_sorted_part_asc(t_stack *head, const size_t part)
{
	while (head && head->part == part)
	{
		if (head->next && head->next->part == part &&
			head->num >= head->next->num)
			return (0);
		head = head->next;
	}
	return (1);
}

_Bool	is_part_in(t_stack *head, const size_t part)
{
	while (head)
	{
		if (head->part == part)
			return (1);
		head = head->next;
	}
	return (0);
}
