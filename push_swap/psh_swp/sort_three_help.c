/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_three_help.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ylila <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/22 20:59:59 by ylila             #+#    #+#             */
/*   Updated: 2020/01/22 21:00:06 by ylila            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	three_und_a_1_0(t_stack **ha, t_stack **hb, const size_t part)
{
	if (size_part(*ha, part) == 2)
		return ;
	rotate(ha, part, 1);
	!hb ? swap(ha, part, 1) : swap_swap(ha, hb, part, 1);
	reverse_rotate(ha, part, 1);
}

void	three_und_a_1_2(t_stack **ha, t_stack **hb, const size_t part)
{
	rotate(ha, part, 1);
	!hb ? swap(ha, part, 1) : swap_swap(ha, hb, part, 1);
	reverse_rotate(ha, part, 1);
	swap(ha, part, 1);
}

void	three_und_a_0_1(t_stack **ha, t_stack **hb, const size_t part)
{
	!hb ? swap(ha, part, 1) : swap_swap(ha, hb, part, 1);
	if (is_sorted_part_asc(*ha, part))
		return ;
	rotate(ha, part, 1);
	swap(ha, part, 1);
	reverse_rotate(ha, part, 1);
}

void	three_und_a_0_2(t_stack **ha, t_stack **hb, const size_t part)
{
	!hb ? swap(ha, part, 1) : swap_swap(ha, hb, part, 1);
	rotate(ha, part, 1);
	swap(ha, part, 1);
	reverse_rotate(ha, part, 1);
	swap(ha, part, 1);
}
