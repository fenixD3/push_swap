/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sorting_intf.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ylila <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/22 20:57:14 by ylila             #+#    #+#             */
/*   Updated: 2020/01/22 21:08:12 by ylila            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

size_t	get_pivot_avg_part(t_stack *head, const size_t scaned_part)
{
	double	total;
	size_t	size;

	total = 0;
	size = size_part(head, scaned_part);
	while (head && head->part == scaned_part)
	{
		total += (double)head->indx;
		head = head->next;
	}
	return (((size_t)ft_floor(total / size)));
}

void	move_from_to(t_stack **from, t_stack **to, const size_t avg_ind)
{
	t_stack	*max_from;

	max_from = find_max_ind_part(*from, NULL, (*from)->part);
	if (size_part(*from, max_from->part) <= 5)
		return (five_sort_prepare(from, to, avg_ind, max_from->part));
	put_parts(*from, max_from->part);
	while ((*from)->name == 'a' ? is_part_in(*from, max_from->part - 1) :
									is_part_in(*from, max_from->part))
	{
		if ((*from)->name == 'a' && (*from)->part == max_from->part - 1)
			push(to, from, 1);
		else if ((*from)->name == 'a')
			rotate(from, max_from->part, 1);
		else if ((*from)->name == 'b' && (*from)->part == max_from->part)
			push(to, from, 1);
		else if ((*from)->name == 'b')
			rotate(from, max_from->part - 1, 1);
	}
	if_from_is_b(from, to, max_from->part);
}

void	if_from_is_b(t_stack **frm, t_stack **to, const size_t max_part)
{
	if ((*frm)->name == 'b')
	{
		if (size_part(*to, max_part) <= 5)
		{
			five_sort_prepare(to, frm, get_pivot_avg_part(*to, max_part),
							max_part);
			while (*frm && (((*frm)->prev->part == max_part - 1 &&
				(*frm)->prev->indx + 1 == (*to)->indx) ||
			((*frm)->part == max_part - 1 && (*frm)->indx + 1 == (*to)->indx)))
			{
				if ((*frm)->prev->part == max_part - 1 &&
						(*frm)->prev->indx + 1 == (*to)->indx)
					reverse_rotate(frm, max_part - 1, 1);
				push(to, frm, 1);
				(*to)->part = 0;
			}
		}
		while (is_another_part(*frm, max_part - 1) &&
				(*frm)->prev->part == max_part - 1)
			reverse_rotate(frm, max_part - 1, 1);
	}
	else
		while (is_another_part(*frm, max_part) &&
				(*frm)->prev->part == max_part)
			reverse_rotate(frm, max_part, 1);
}

void	five_sort_prepare(t_stack **from, t_stack **to, const size_t avg_ind,
						const size_t max_part)
{
	_Bool	size_equal;

	size_equal = size_stack(*from) == size_part(*from, max_part);
	if ((*from)->name == 'a' && size_equal)
		five_sort_a(from, to, avg_ind, max_part);
	else if ((*from)->name == 'a' && !size_equal)
		five_sort_part_a(from, to, avg_ind, max_part);
	else if ((*from)->name == 'b' && size_equal)
		five_sort_b(from, to, size_stack(*from) == 4 ? avg_ind + 1 : avg_ind,
					max_part);
	else if ((*from)->name == 'b' && !size_equal)
		five_sort_part_b(from, to,
		size_part(*from, max_part) == 4 ? avg_ind + 1 : avg_ind, max_part);
	if (!(*from) || (*from)->name == 'b')
		sorted_part_to_zero(to, max_part);
	else
		sorted_part_to_zero(from, max_part);
}
