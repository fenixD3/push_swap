/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ylila <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/21 23:14:51 by ylila             #+#    #+#             */
/*   Updated: 2020/01/21 23:14:53 by ylila            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "validate_input.h"
#include "push_swap.h"

void	general_sorting(t_stack **head_a, t_stack **head_b)
{
	t_stack	*max_part;
	t_stack	**from;
	t_stack	**to;

	if (is_sorted_asc(*head_a) && is_empty(*head_b))
		return ;
	max_part = find_max_part(*head_a, *head_b);
	from = (max_part->name == 'a') ? head_a : head_b;
	to = ((*from)->name == 'a') ? head_b : head_a;
	move_from_to(from, to, get_pivot_avg_part(*from, max_part->part));
	if (!(*from) || (*from)->name == 'b')
		general_sorting(to, from);
	else
		general_sorting(from, to);
}

/*int		main(int ac, char **av)
{
	t_stack	*head_a;
	t_stack	*head_b;

	head_a = NULL;
	head_b = NULL;
	prepare_args(ac, av, &head_a, NULL);
	make_rank(head_a);
	general_sorting(&head_a, &head_b);
	free_stack(&head_a);
	free_stack(&head_b);
	return (0);
}*/
