/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_both_rnd_three.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ylila <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/22 20:58:14 by ylila             #+#    #+#             */
/*   Updated: 2020/01/22 21:07:54 by ylila            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void		both_round_three(t_stack **ha, t_stack **hb, const size_t part)
{
	size_t	pos_max;
	size_t	pos_min;

	find_max_ind_part(*ha, &pos_max, (*ha)->part);
	find_min_ind_part(*ha, &pos_min, (*ha)->part);
	if (pos_max == 1 && pos_min == 0)
	{
		reverse_rotate(ha, part, 1);
		swap_swap(ha, hb, part, 1);
	}
	else if (pos_max == 2 && pos_min == 1)
		swap_swap(ha, hb, part, 1);
	else if (pos_max == 1 && pos_min == 2)
		reverse_rotate_rotate(ha, hb, part, 1);
	else if (pos_max == 0 && pos_min == 1)
		rotate_rotate(ha, hb, part, 1);
	else if (pos_max == 0 && pos_min == 2)
	{
		swap_swap(ha, hb, part, 1);
		reverse_rotate(ha, part, 1);
	}
}

/*
** 3 elem into a
*/

void		both_round_three_under_b_is_2(t_stack **ha, t_stack **hb,
									const size_t part)
{
	size_t	pos_max;
	size_t	pos_min;

	find_max_ind_part(*ha, &pos_max, (*ha)->part);
	find_min_ind_part(*ha, &pos_min, (*ha)->part);
	if (pos_max == 1 && pos_min == 0)
	{
		reverse_rotate(ha, part, 1);
		swap_swap(ha, hb, part, 1);
	}
	else if (pos_max == 2 && pos_min == 1)
		swap_swap(ha, hb, part, 1);
	else if (pos_max == 1 && pos_min == 2)
		reverse_rotate(ha, part, 1);
	else if (pos_max == 0 && pos_min == 1)
		rotate(ha, part, 1);
	else if (pos_max == 0 && pos_min == 2)
	{
		swap_swap(ha, hb, part, 1);
		reverse_rotate(ha, part, 1);
	}
	if ((pos_max == 1 && pos_min == 2) || (pos_max == 0 && pos_min == 1))
		swap(hb, part, 1);
}

static void	adding_swap_a(const size_t pos_max, const size_t pos_min,
							t_stack **two, const size_t part)
{
	if ((pos_max == 1 && pos_min == 0) || (pos_max == 2 && pos_min == 1))
		swap(two, part, 1);
}

/*
** 3 elem into b
*/

void		both_round_three_under_a_is_2(t_stack **three, t_stack **two,
								const size_t part)
{
	size_t	pos_max;
	size_t	pos_min;

	find_max_ind_part(*three, &pos_max, (*three)->part);
	find_min_ind_part(*three, &pos_min, (*three)->part);
	if (pos_max == 2 && pos_min == 0)
	{
		swap_swap(two, three, part, 1);
		reverse_rotate(three, part, 1);
	}
	else if (pos_max == 1 && pos_min == 0)
	{
		reverse_rotate(three, part, 1);
		reverse_rotate(three, part, 1);
	}
	else if (pos_max == 2 && pos_min == 1)
		reverse_rotate(three, part, 1);
	else if (pos_max == 1 && pos_min == 2)
		swap_swap(two, three, part, 1);
	else if (pos_max == 0 && pos_min == 1)
	{
		reverse_rotate(three, part, 1);
		swap_swap(two, three, part, 1);
	}
	adding_swap_a(pos_max, pos_min, two, part);
}

void		both_round_three_under_a(t_stack **ha, t_stack **hb,
								const size_t part)
{
	size_t	pos_max;
	size_t	pos_min;

	find_max_ind_part(*ha, &pos_max, (*ha)->part);
	find_min_ind_part(*ha, &pos_min, (*ha)->part);
	if (pos_max == 1 && pos_min == 0)
		three_und_a_1_0(ha, hb, part);
	else if (pos_max == 2 && pos_min == 1)
		swap_swap(ha, hb, part, 1);
	else if (pos_max == 1 && pos_min == 2)
		three_und_a_1_2(ha, hb, part);
	else if (pos_max == 0 && pos_min == 1)
		three_und_a_0_1(ha, hb, part);
	else if (pos_max == 0 && pos_min == 2)
		three_und_a_0_2(ha, hb, part);
}
