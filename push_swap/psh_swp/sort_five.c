/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_five.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ylila <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/22 20:55:03 by ylila             #+#    #+#             */
/*   Updated: 2020/01/22 21:09:04 by ylila            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void		push_n_time(t_stack **dest, t_stack **source, size_t size_part,
						_Bool out)
{
	while (size_part--)
		push(dest, source, out);
}

void		five_sort_a(t_stack **from, t_stack **to, const size_t avg_ind,
				const size_t part)
{
	size_t	size;

	if (size_stack(*from) <= 3)
		return (three_sort(from, part));
	size = size_stack(*from);
	while (size_stack(*from) > 3 && size--)
	{
		if ((*from)->indx < avg_ind)
			push(to, from, 1);
		else
			rotate(from, part, 1);
	}
	(*from)->is_sort = is_sorted_asc(*from);
	(*to)->is_sort = is_sorted_part_asc(*to, (*to)->part);
	if ((*from)->is_sort && (*to)->is_sort)
		swap(to, part, 1);
	else if (!(*from)->is_sort && !(*to)->is_sort)
		three_sort(from, part);
	else if (!(*from)->is_sort && (*to)->is_sort)
		!is_another_part(*to, (*to)->part) ? both_round_three(from, to, part) :
		both_round_three_under_b_is_2(from, to, part);
	push_n_time(from, to, size_part(*to, part), 1);
}

void		five_sort_part_a(t_stack **from, t_stack **to, const size_t avg_ind,
							const size_t part)
{
	size_t	sz_prt;

	if (size_part(*from, part) <= 3)
		return (three_sort_under_a(from, part));
	sz_prt = size_part(*from, part);
	while (size_part(*from, part) > 3 && sz_prt--)
	{
		if ((*from)->indx < avg_ind)
			push(to, from, 1);
		else
			rotate(from, part, 1);
	}
	while ((*from)->prev->part == part)
		reverse_rotate(from, part, 1);
	(*from)->is_sort = is_sorted_part_asc(*from, part);
	(*to)->is_sort = is_sorted_part_asc(*to, part);
	if ((*from)->is_sort && (*to)->is_sort)
		swap(to, part, 1);
	else if (!(*from)->is_sort && !(*to)->is_sort)
		three_sort_under_a(from, part);
	else if (!(*from)->is_sort && (*to)->is_sort)
		both_round_three_under_a(from, to, part);
	push_n_time(from, to, size_part(*to, part), 1);
}

void		five_sort_b(t_stack **from, t_stack **to, const size_t avg_ind,
				const size_t part)
{
	size_t	size;

	if (size_stack(*from) <= 3)
		return (three_reverse_sort(from, to, part));
	size = size_stack(*from);
	while (size_stack(*from) > 3 && size--)
	{
		if ((*from)->indx > avg_ind)
			push(to, from, 1);
		else
			rotate(from, part, 1);
	}
	(*from)->is_sort = is_sorted_desc(*from);
	(*to)->is_sort = is_sorted_part_asc(*to, (*to)->part);
	if ((*from)->is_sort && !(*to)->is_sort)
		swap(to, part, 1);
	else if (!(*from)->is_sort && (*to)->is_sort)
		return (three_reverse_sort(from, to, part));
	else if (!(*from)->is_sort && !(*to)->is_sort)
		both_round_three_under_a_is_2(from, to, part);
	push_n_time(to, from, size_part(*from, part), 1);
}

void		five_sort_part_b(t_stack **from, t_stack **to, const size_t avg_ind,
							const size_t part)
{
	size_t	sz_prt;

	if (size_part(*from, part) <= 3)
		return (three_sort_under_b(from, to, size_part(*from, part), part));
	sz_prt = size_part(*from, part);
	while (size_part(*from, part) >= 3 && sz_prt--)
	{
		if ((*from)->indx >= avg_ind)
			push(to, from, 1);
		else
			rotate(from, part, 1);
	}
	while ((*from)->prev->part == part)
		reverse_rotate(from, part, 1);
	(*from)->is_sort = is_sorted_part_desc(*from, part);
	(*to)->is_sort = is_sorted_part_asc(*to, part);
	if (!(*from)->is_sort && (*to)->is_sort)
		swap(from, part, 1);
	else if ((*from)->is_sort && !(*to)->is_sort)
		three_sort_under_a(to, part);
	else if (!(*from)->is_sort && !(*to)->is_sort)
		both_round_three_under_a(to, from, part);
	push_n_time(to, from, size_part(*from, part), 1);
}
