/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   composite_instructions.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ylila <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/21 23:11:41 by ylila             #+#    #+#             */
/*   Updated: 2020/01/21 23:12:25 by ylila            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack_intf.h"

void	swap_swap(t_stack **head_a, t_stack **head_b, const size_t part,
					_Bool out)
{
	swap(head_a, part, 0);
	swap(head_b, part, 0);
	if (out)
		ft_printf("ss\n");
}

void	rotate_rotate(t_stack **head_a, t_stack **head_b, const size_t part,
					_Bool out)
{
	rotate(head_a, part, 0);
	rotate(head_b, part, 0);
	if (out)
		ft_printf("rr\n");
}

void	reverse_rotate_rotate(t_stack **head_a, t_stack **head_b,
						const size_t part, _Bool out)
{
	reverse_rotate(head_a, part, 0);
	reverse_rotate(head_b, part, 0);
	if (out)
		ft_printf("rrr\n");
}
