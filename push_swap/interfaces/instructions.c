/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   instructions.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ylila <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/21 23:12:32 by ylila             #+#    #+#             */
/*   Updated: 2020/01/21 23:12:38 by ylila            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	swap(t_stack **first, const size_t part, _Bool out)
{
	size_t	size;
	t_stack	*second;

	if ((size = size_part(*first, part)) == 0 || size == 1)
		return ;
	second = (*first)->next;
	second->next ? second->next->prev = *first : second;
	second->prev = (*first)->prev;
	(*first)->prev = second;
	(*first)->next = second->next;
	second->next = *first;
	*first = second;
	if (out)
		ft_printf("s%c\n", (*first)->name);
}

void	push(t_stack **dest, t_stack **source, _Bool out)
{
	t_stack	*new_src;

	if (is_empty(*source))
		return ;
	new_src = (*source)->next;
	(*source)->next ? (*source)->next->prev = (*source)->prev : *source;
	(*source)->next = *dest;
	(*source)->prev = (!*dest) ? *source : (*dest)->prev;
	(*dest) ? (*dest)->prev = *source : *dest;
	*dest = *source;
	*source = new_src;
	(*dest)->name = ((*dest)->name == 'a') ? 'b' : 'a';
	if (out)
		ft_printf("p%c\n", (*dest)->name);
}

void	rotate(t_stack **head, const size_t part, _Bool out)
{
	t_stack	*second;
	size_t	size;

	if ((size = size_part(*head, part)) == 1 || !size)
		return ;
	second = (*head)->next;
	(*head)->prev->next = *head;
	(*head)->next = NULL;
	*head = second;
	if (out)
		ft_printf("r%c\n", (*head)->name);
}

void	reverse_rotate(t_stack **head, const size_t part, _Bool out)
{
	t_stack	*last;
	size_t	size;

	if (((size = size_part(*head, part)) == 1 || !size) &&
				(*head)->part == part)
		return ;
	last = (*head)->prev;
	last->prev->next = NULL;
	last->next = *head;
	*head = last;
	if (out)
		ft_printf("rr%c\n", (*head)->name);
}
