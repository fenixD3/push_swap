/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_intf_2.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ylila <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/21 23:13:04 by ylila             #+#    #+#             */
/*   Updated: 2020/01/21 23:13:07 by ylila            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack_intf.h"
#include "ft_ptintf.h"

size_t	size_stack(t_stack *head)
{
	size_t	size;

	size = 0;
	while (head)
	{
		++size;
		head = head->next;
	}
	return (size);
}

_Bool	is_empty(t_stack *head)
{
	return (!size_stack(head));
}

void	free_stack(t_stack **head)
{
	t_stack *next;

	while (*head)
	{
		next = (*head)->next;
		free(*head);
		*head = next;
	}
}

void	print_stacks(t_stack *head_a, t_stack *head_b)
{
	while (head_a || head_b)
	{
		if (head_a)
		{
			ft_printf("%-15d|", head_a->num);
			head_a = head_a->next;
		}
		else
			ft_printf("%15s", " ");
		if (head_b)
		{
			ft_printf("%-15d\n", head_b->num);
			head_b = head_b->next;
		}
		else
			ft_printf("%15s\n", " ");
	}
	ft_printf("--------------------------------\n");
	ft_printf("%-15s|%-15s\n", "a", "b");
	ft_printf("--------------------------------\n");
}
