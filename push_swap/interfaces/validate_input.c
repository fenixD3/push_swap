/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   validate_input.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ylila <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/21 23:13:12 by ylila             #+#    #+#             */
/*   Updated: 2020/01/21 23:13:14 by ylila            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "validate_input.h"
#include <limits.h>
#include "ft_ptintf.h"

inline void	print_error(_Bool ko)
{
	if (!ko)
		ft_printf("Error\n");
	else
		ft_printf("KO\n");
	exit(1);
}

_Bool		is_debug_flag(const char *arg, uint8_t *flags, size_t *len_flg)
{
	if (ft_strcmp(arg, "-v"))
		return (0);
	*flags |= FLG_V;
	if (len_flg)
		*len_flg = ft_strlen("-v");
	return (1);
}

size_t		parse_arg(const char *arg, t_stack **head_a)
{
	long long	num;

	num = ft_atol(arg);
	if ((*arg != '-' && !ft_isdigit(*arg)) || *(arg + len_num(num)) != '\0' ||
		(num > INT_MAX || num < INT_MIN) || !push_back(head_a, num, 'a'))
	{
		free_stack(head_a);
		print_error(0);
	}
	return (len_num(num));
}

void		parse_one_str(char *str, t_stack **head_a, uint8_t *flags)
{
	size_t	i;
	size_t	len_flg;

	i = 0;
	while (*str)
	{
		while (*str == ' ')
			++str;
		if (flags)
			if (!*head_a && ++i <= FLG_COUNT &&
					is_debug_flag(str, flags, &len_flg))
			{
				str += len_flg;
				continue ;
			}
		if (*str)
			str += parse_arg(str, head_a);
	}
}

void		prepare_args(int ac, char **av, t_stack **head_a, uint8_t *flags)
{
	int	i;

	i = 0;
	if (ac <= 1)
		print_error(0);
	else if (ac == 2)
		parse_one_str(av[1], head_a, flags);
	else
		while (++i < ac)
		{
			if (flags)
				if (!*head_a && i <= FLG_COUNT && is_debug_flag(av[i], flags,
						NULL))
					continue ;
			parse_arg(av[i], head_a);
		}
}
