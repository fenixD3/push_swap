/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_intf.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ylila <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/21 23:12:43 by ylila             #+#    #+#             */
/*   Updated: 2020/01/21 23:12:45 by ylila            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack_intf.h"

t_stack	*create_new(const int n_num, const char n_name)
{
	t_stack	*new;

	if (!(new = (t_stack *)malloc(sizeof(t_stack))))
		return (NULL);
	new->num = n_num;
	new->indx = 0;
	new->part = 0;
	new->next = NULL;
	new->prev = NULL;
	new->name = n_name;
	return (new);
}

_Bool	push_back(t_stack **head, const int n_num, const char n_name)
{
	t_stack	*pb;

	if (!(*head))
	{
		if (!(*head = create_new(n_num, n_name)))
			return (0);
		(*head)->prev = *head;
	}
	else
	{
		pb = *head;
		while (pb->next)
		{
			if (pb->num == n_num)
				return (0);
			pb = pb->next;
		}
		if (pb->num == n_num || !(pb->next = create_new(n_num, n_name)))
			return (0);
		pb->next->prev = pb;
		(*head)->prev = pb->next;
	}
	return (1);
}

_Bool	is_sorted_asc(t_stack *head)
{
	while (head)
	{
		if (head->next && head->num >= head->next->num)
			return (0);
		head = head->next;
	}
	return (1);
}

_Bool	is_sorted_desc(t_stack *head)
{
	while (head)
	{
		if (head->next && head->num <= head->next->num)
			return (0);
		head = head->next;
	}
	return (1);
}
