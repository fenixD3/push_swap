/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   validate_input.h                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ylila <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/21 23:13:27 by ylila             #+#    #+#             */
/*   Updated: 2020/01/22 21:10:16 by ylila            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VALIDATE_INPUT_H
# define VALIDATE_INPUT_H

# include "stack_intf.h"
# include "libft.h"

# define FLG_COUNT 1
# define FLG_V 1u

size_t	parse_arg(const char *arg, t_stack **head_a);
void	parse_one_str(char *str, t_stack **head_a, uint8_t *flags);
void	prepare_args(int ac, char **av, t_stack **head_a, uint8_t *flags);
void	print_error(_Bool ko);
_Bool	is_debug_flag(const char *arg, uint8_t *flags, size_t *len_flg);

#endif
