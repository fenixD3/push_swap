/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_intf.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ylila <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/21 23:12:51 by ylila             #+#    #+#             */
/*   Updated: 2020/01/22 21:15:59 by ylila            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STACK_INTF_H
# define STACK_INTF_H

# include <stdlib.h>
# include "ft_ptintf.h"

typedef struct	s_stack
{
	int				num;
	size_t			indx;
	size_t			part;
	char			name;
	_Bool			is_sort;
	struct s_stack	*next;
	struct s_stack	*prev;
}				t_stack;

t_stack			*create_new(const int n_num, const char n_name);
_Bool			push_back(t_stack **head, const int n_num, const char n_name);
size_t			size_stack(t_stack *head);
_Bool			is_empty(t_stack *head);
void			free_stack(t_stack **head);
_Bool			is_sorted_asc(t_stack *head);
_Bool			is_sorted_desc(t_stack *head);
void			print_stacks(t_stack *head_a, t_stack *head_b);

void swap(t_stack **first, const size_t part, _Bool out);
void push(t_stack **dest, t_stack **source, _Bool out);
void rotate(t_stack **head, const size_t part, _Bool out);
void reverse_rotate(t_stack **head, const size_t part, _Bool out);
void			swap_swap(t_stack **head_a, t_stack **head_b,
						const size_t part, _Bool out);
void			rotate_rotate(t_stack **head_a, t_stack **head_b,
						const size_t part, _Bool out);
void			reverse_rotate_rotate(t_stack **head_a, t_stack **head_b,
						const size_t part, _Bool out);

#endif
